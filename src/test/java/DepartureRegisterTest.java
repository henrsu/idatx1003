import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.model.DepartureRegister;
import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


/**
 * This is a class for testing of the departure register.
 * Contains negative and positive tests for various methods of the DepartureRegister class.
 *
 * @author Henrik Sund
 */
class DepartureRegisterTest {

  DepartureRegister register = new DepartureRegister();

  @Nested
  @DisplayName("Negative tests for 'DepartureRegister', throws exceptions"
          + " on wrong input parameters")
  public class MethodsThrowsExceptions {

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.'"
            + " on blank departure time")
    public void addDepartureBlankDepartureTimeTest() {
      try {
        register.addDeparture(null,  // departureTime
                "L2",               // line
                "45",                // trainNumber
                "Bergen",               // destination
                2,                     // track
                LocalTime.of(0, 15)); //Delay
        fail("The method 'addDepartureTBlankDepartureTimeTest' did not "
                + "throw on blank departureTime");
      } catch (IllegalArgumentException e) {
        assertEquals("The LocalTime for the parameter 'Departure Time' was null,"
                + " please provide a valid time.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.'"
            + " on blank string ")
    public void addDepartureBlankLineTest() {
      try {
        register.addDeparture(LocalTime.of(8, 55),  // departureTime
                "",               // line
                "45",                // trainNumber
                "Bergen",               // destination
                2,                     // track
                LocalTime.of(0, 15)); // delay
        fail("The method 'addDepartureOnBlankLineTest' did not throw on blank string Line");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Line' was a blank string, please "
                + "retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.' "
            + "on blank string trainNumber")
    public void addDepartureBlankTrainNumberTest() {
      try {
        register.addDeparture(LocalTime.of(8, 55),  // departureTime
                "L2",               // line
                "",                // trainNumber
                "Bergen",               // destination
                2,                     // track
                LocalTime.of(0, 15)); // delay
        fail("The method 'addDepartureBlankTrainNumberTest' did not throw "
                + "on blank train number");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Train number' was a blank string,"
                + " please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.' "
            + "on blank string destination")
    public void addDepartureBlankDestinationTest() {
      try {
        register.addDeparture(LocalTime.of(8, 55),  // departureTime
                "L2",               // line
                "602",                // trainNumber
                "",               // destination
                2,                     // track
                LocalTime.of(0, 15)); // delay
        fail("The method 'addDepartureBlankDestinationTest' did not throw"
                + " on blank destination");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Destination' was a blank string,"
                + " please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.' "
            + "on negative int track")
    public void addDepartureNegativeTrackTest() {
      try {
        register.addDeparture(LocalTime.of(8, 55),  // departureTime
                "L2",               // line
                "602",                // trainNumber
                "Oslo",               // destination
                -6,                     // track
                LocalTime.of(0, 15)); // delay
        fail("The method 'addDepartureNegativeTrackTest' did not throw on"
                + " blank track");
      } catch (IllegalArgumentException e) {
        assertEquals("The parameter 'Track' must be a positive integer, from 0 - 4",
                e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.' "
            + "on illegal int track")
    public void addDepartureIllegalTrackTest() {
      try {
        register.addDeparture(LocalTime.of(8, 55),  // departureTime
                "L2",               // line
                "602",                // trainNumber
                "Oslo",               // destination
                6,                     // track
                LocalTime.of(0, 15)); // delay
        fail("The method 'addDepartureIllegalTrackTest' did not throw on illegal track");
      } catch (IllegalArgumentException e) {
        assertEquals("The parameter 'Track' must be a positive integer, from 0 - 4",
                e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.'"
            + " on blank delay")
    public void addDepartureBlankDelayTest() {
      try {
        register.addDeparture(LocalTime.of(8, 30),  // departureTime
                "L2",               // line
                "45",                // trainNumber
                "Bergen",               // destination
                2,                     // track
                null); // delay
        fail("The method 'addDepartureBlankDelayTest' did not "
                + "throw on blank delay");
      } catch (IllegalArgumentException e) {
        assertEquals("The LocalTime for the parameter 'Delay' was null, "
                + "please provide a valid time.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister addDeparture() throws 'Ill.Arg.Exc.' on already registered "
            + "train number")
    public void addDepartureTrainNumberDuplicateTest() {
      try {
        register.addDeparture(
                LocalTime.of(18, 0), // departureTime
                "L1",          // line
                "602",             // trainNumber
                "Oslo",            // destination
                1,                  // track
                LocalTime.of(0, 15)); //Delay
        register.addDeparture(
                LocalTime.of(8, 55),  // departureTime
                "F4",               // line
                "45",                // trainNumber
                "Bergen",               // destination
                2,                     // track
                LocalTime.of(0, 15));  // delay

        register.addDeparture(
                LocalTime.of(12, 45),  // departureTime
                "L2",             // line
                "602",                // trainNumber
                "Trondheim",               // destination
                0,                     // track
                LocalTime.of(0, 30));     // delay

        fail("The method 'addDepartureTrainNumberDuplicateTest'"
                + " did not throw when the train number was already in the register");
      } catch (IllegalArgumentException e) {
        assertEquals("A departure with the same train number is already in the"
                + " register Please try again with a different train number.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister trainNumberSearch() throws 'Ill.Arg.Exc.' "
            + "on non-existent train number")
    public void trainNumberSearchNonexistentTrainNumberTest() {
      try {
        register.trainNumberSearch("999");
        fail("The method 'trainNumberSearchNonexistentTrainNumberTest' did not"
                + " throw on non-existent train number");
      } catch (IllegalArgumentException e) {
        assertEquals("No train with the specified train number exists in the register. "
                + "Please try again with a different train number.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister trainDestinationSearch() throws 'Ill.Arg.Exc.' "
            + "on non-existent destination")
    public void trainDestinationSearchNonexistentDestinationTest() {
      try {
        register.trainDestinationSearch("NonexistentCity");
        fail("The method 'trainDestinationSearchNonexistentDestinationTest' "
                + "did not throw on non-existent destination");
      } catch (IllegalArgumentException e) {
        assertEquals("No train with the specified destination exists in the register. "
                + "Please try again with a different destination.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister trainLineSearch() throws 'Ill.Arg.Exc.' "
            + "on non-existent line")
    public void trainLineSearchNonexistentLineTest() {
      try {
        register.trainLineSearch("NonexistentCity");
        fail("The method 'trainLineSearchNonexistentLineTest' "
                + "did not throw on non-existent line");
      } catch (IllegalArgumentException e) {
        assertEquals("No train with the specified line exists in the "
                + "register. Please try again with a different line.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister updateCurrentTime() throws 'Ill.Arg.Exc.' "
            + " on past time")
    public void updateCurrentTimeTest() {
      register.setUpdateCurrentTime(LocalTime.of(LocalTime.now().getHour(),
              LocalTime.now().getMinute()));
      try {
        register.setUpdateCurrentTime(LocalTime.now().minusHours(2));
        fail("The method 'updateCurrentTimeTest' did not throw on past time");
      } catch (IllegalArgumentException e) {
        assertEquals("You can't turn back the time, current time is: " + LocalTime
                .of(LocalTime.now().getHour(), LocalTime.now().getMinute()), e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister removePastDepartures removes past departures")
    public void removePastDeparturesTest() {
      register.addDeparture(LocalTime.now().minusHours(2), "L1",
              "601", "Oslo", 1, LocalTime.of(0, 15));
      register.addDeparture(LocalTime.now().minusHours(1), "L2",
              "602", "Bergen", 2, LocalTime.of(3, 15));
      int initialSize = register.getDepartureRegisterList().size();
      register.setUpdateCurrentTime(LocalTime.of(LocalTime.now().getHour(),
              LocalTime.now().getMinute()));
      register.removePastDepartures();
      assertNotEquals(initialSize, register.getDepartureRegisterList().size());
    }


    @Test
    @DisplayName("DepartureRegister getLastDeparture() throws 'Ill.Arg.Exc.' "
            + " if the departure register is empty")
    public void getLastDepartureTest() {
      assertThrows(IllegalArgumentException.class, register::getLastDeparture);
    }

    @Test
    @DisplayName("DepartureRegister editTrack() throws 'Ill.Arg.Exc.' "
            + " on colliding departures")
    public void editTrackNoCollidingDeparturesTest() {
      try {
        register.addDeparture(LocalTime.of(3, 0), "L1",
                "601", "Oslo", 1, LocalTime.of(0, 15));
        register.addDeparture(LocalTime.of(3, 0), "L2",
                "611", "Oslo", 2, LocalTime.of(0, 15));
        register.editTrack("601", 2);
        fail("The method 'editTrackNoCollidingDepartures' "
                + "did not throw on departures with the same track and departure time");
      } catch (IllegalArgumentException e) {
        assertEquals("A departure with the same track and"
                + " adjusted departureTime is already in the register. \n"
                + "Try again with a different departure time, track, or delay.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister editDelay() throws 'Ill.Arg.Exc.' "
            + " on colliding departure track")
    public void editDelayNoCollidingDepartureTrackTest() {
      try {
        register.addDeparture(LocalTime.of(3, 0), "L1",
                "601", "Oslo", 1, LocalTime.of(0, 15));
        register.addDeparture(LocalTime.of(3, 0), "L2",
                "611", "Oslo", 1, LocalTime.of(0, 0));
        register.editDelay("601", LocalTime.of(0, 0));
        fail("The method 'departureRegisterEditDelay  NoCollidingDepartureTrack' "
                + "did not throw on departures with the same track and departure time");
      } catch (IllegalArgumentException e) {
        assertEquals("A departure with the same track and"
                + " adjusted departureTime is already in the register. \n"
                + "Try again with a different departure time, track, or delay.", e.getMessage());
      }
    }

    @Test
    @DisplayName("DepartureRegister editDelay() throws 'Ill.Arg.Exc.' "
            + " on colliding departure line")
    public void editDelayNoCollidingDepartureLineTest() {
      try {
        register.addDeparture(LocalTime.of(3, 0), "L1",
                "601", "Oslo", 1, LocalTime.of(0, 15));
        register.addDeparture(LocalTime.of(3, 0), "L1",
                "611", "Oslo", 2, LocalTime.of(0, 0));
        register.editDelay("601", LocalTime.of(0, 0));
        fail("The method 'editDelayNoCollidingDepartureLine' "
                + "did not throw on departures with the same line and departure time");
      } catch (IllegalArgumentException e) {
        assertEquals("A departure with the same line and"
                + " adjusted departureTime is already in the register. \n"
                + "Try again with a different departure time, line, or delay.", e.getMessage());
      }
    }
  }

  /**
   * Nested class for positive tests, which check that methods do not throw exceptions
   * and return expected values.
   */
  @Nested
  @DisplayName("Positive tests for TrainDeparture methods")
  public class MethodsDoNotThrowException {

    @Test
    @DisplayName("DepartureRegister addDeparture() adds a departure successfully")
    public void addDepartureAddsSuccessfullyTest() {
      register.addDeparture(LocalTime.of(8, 30), "L1", "601",
              "Oslo", 2, LocalTime.of(0, 15));
      assertEquals(1, register.getDepartureRegisterList().size());
    }


    @Test
    @DisplayName("DepartureRegister printAllDepartures() works successfully")
    public void printAllDeparturesWorksSuccessfullyTest() {
      register.addDeparture(LocalTime.now().plusHours(1), "L1", "601",
              "Oslo", 2, LocalTime.of(0, 15));
      register.addDeparture(LocalTime.now().plusHours(1), "L2", "602",
              "Bergen", 1, LocalTime.of(0, 10));
      register.printAllDepartures();
    }

    @Test
    @DisplayName("DepartureRegister trainNumberSearch() finds existing train number")
    public void trainNumberSearchFindsExistingTrainNumberTest() {
      register.addDeparture(LocalTime.of(8, 30), "L1", "601", "Oslo", 2, LocalTime.of(0, 15));
      assertDoesNotThrow(() -> register.trainNumberSearch("601"));
    }

    @Test
    @DisplayName("DepartureRegister trainDestinationSearch() finds existing destination")
    public void trainDestinationSearchFindsExistingDestination() {
      
      register.addDeparture(LocalTime.of(8, 30), "L1", "601", "Oslo", 2, LocalTime.of(0, 15));
      assertDoesNotThrow(() -> register.trainDestinationSearch("Oslo"));
    }

    @Test
    @DisplayName("DepartureRegister trainLineSearch() finds existing line")
    public void trainLineSearchFindsExistingLineTest() {
      register.addDeparture(LocalTime.now().plusHours(1), "L1", "601",
              "Oslo", 2, LocalTime.of(0, 15));
      assertDoesNotThrow(() -> register.trainLineSearch("L1"));
    }

    @Test
    @DisplayName("DepartureRegister updateCurrentTime() works successfully")
    public void updateCurrentTimeWorksSuccessfullyTest() {
      assertDoesNotThrow(() -> register.setUpdateCurrentTime(LocalTime.of(LocalTime.now().getHour(),
              LocalTime.now().getMinute())));
    }

    @Test
    @DisplayName("DepartureRegister removePastDepartures() works successfully")
    public void removePastDeparturesWorksSuccessfullyTest() {
      register.addDeparture(LocalTime.now().plusHours(2), "L1",
              "601", "Oslo", 1, LocalTime.of(0, 15));
      register.addDeparture(LocalTime.now().plusHours(1), "L2",
              "602", "Bergen", 2, LocalTime.of(0, 15));

      int initialSize = register.getDepartureRegisterList().size();
      register.setUpdateCurrentTime(LocalTime.of(LocalTime.now().getHour(),
              LocalTime.now().getMinute()));
      assertDoesNotThrow(register::removePastDepartures);
      assertEquals(initialSize, register.getDepartureRegisterList().size());
    }

    @Test
    @DisplayName("DepartureRegister getLastDeparture() works successfully")
    public void GetLastDepartureWorksSuccessfully() {
      
      register.addDeparture(LocalTime.now().plusHours(2), "L1",
              "601", "Oslo", 1, LocalTime.of(0, 15));
      assertDoesNotThrow(register::getLastDeparture);
      assertEquals(1, register.getLastDeparture().size());
    }

    @Test
    @DisplayName("DepartureRegister editTrack() works successfully")
    public void editTrackWorksSuccessfullyTest() {
      register.addDeparture(LocalTime.now().plusHours(2), "L1",
              "601", "Oslo", 1, LocalTime.of(0, 15));
      register.addDeparture(LocalTime.now().plusHours(2), "L3",
              "6201", "Oslo", 2, LocalTime.of(0, 15));
      assertDoesNotThrow(() -> register.editTrack("601", 4));
    }

    @Test
    @DisplayName("DepartureRegister editDelay() works successfully")
    public void editDelayWorksSuccessfullyTest() {
      register.addDeparture(LocalTime.now().plusHours(2), "L1",
              "601", "Oslo", 1, LocalTime.of(0, 0));
      register.addDeparture(LocalTime.now().plusHours(2), "L3",
              "6201", "Oslo", 1, LocalTime.of(0, 15));
      assertDoesNotThrow(() -> register.editDelay("601", LocalTime.of(3, 3)));
    }




  }
}
