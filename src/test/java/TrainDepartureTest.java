import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.stud.model.TrainDeparture;
import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for the TrainDeparture class.
 * Contains negative and positive tests for various methods from the TrainDeparture class.
 *
 * @author Henrik Sund
 *
 */
public class TrainDepartureTest {

  /**
   * Nested class for negative tests, which check if exceptions
   * are thrown on invalid input parameters.
   */
  @Nested
  @DisplayName("Negative tests for 'TrainDeparture', throws exceptions on wrong input parameters")
  public class MethodsThrowsExceptions  {

    /**
     * Tests the TrainDeparture constructor for throwing 'Ill.Arg.Exc.' on null departure time.
     *
     * <p>Verifies that the TrainDeparture constructor throws an IllegalArgumentException
     * when attempting to create an instance with a null departure time. It checks the expected
     * exception message.
     */
    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on null departure time")
    public void trainDepartureConstructorThrowsOnNullDepartureTime() {
      try {
        new TrainDeparture(null, "Line1", "123", "Destination1", 1, LocalTime.of(0, 15));
        fail("The test 'trainDepartureConstructorThrowsOnNullDepartureTime' failed since"
                + " it did not throw 'Ill.Arg.Exc.' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The LocalTime for the parameter 'Departure Time' was null,"
                + " please provide a valid time.", e.getMessage());
      }
    }

    /**
     * Tests the TrainDeparture constructor for throwing 'Ill.Arg.Exc.' on blank line.
     *
     * <p>Verifies that the TrainDeparture constructor throws an IllegalArgumentException
     * when attempting to create an instance with a blank line. It checks the expected
     * exception message.
     */
    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on blank line")
    public void trainDepartureConstructorThrowsOnBlankLine() {
      try {
        LocalTime departureTime = LocalTime.of(12, 30);
        new TrainDeparture(departureTime, "", "123", "Destination1", 1, LocalTime.of(0, 15));
        fail("The test 'trainDepartureConstructorThrowsOnBlankLine' failed "
                + "since it did not throw 'Ill.Arg.Exc.' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Line' was a blank string,"
                + " please retry registration.", e.getMessage());
      }
    }

    /**
     * Tests the TrainDeparture constructor for throwing 'Ill.Arg.Exc.' on blank destination.
     *
     * <p>Verifies that the TrainDeparture constructor throws an IllegalArgumentException
     * when attempting to create an instance with a blank destination.
     */
    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on blank destination")
    public void trainDepartureConstructorThrowsOnBlankDestination() {
      try {
        LocalTime departureTime = LocalTime.of(12, 30);
        new TrainDeparture(departureTime, "Line1", "123", "", 1, LocalTime.of(0, 15));
        fail("The test 'trainDepartureConstructorThrowsOnBlankDestination' "
                + "failed since it did not throw 'Ill.Arg.Exc.' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Destination' was a blank string,"
                + " please retry registration.", e.getMessage());
      }
    }

    /**
     * Tests the TrainDeparture constructor for throwing 'Ill.Arg.Exc.' on blank train number.
     *
     * <p>Verifies that the TrainDeparture constructor throws an IllegalArgumentException
     * when attempting to create an instance with a blank train number.
     */
    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on blank train number")
    public void trainDepartureConstructorThrowsOnBlankTrainNumber() {
      try {
        LocalTime departureTime = LocalTime.of(12, 30);
        new TrainDeparture(departureTime, "Line1", "", "Destination1", 1, LocalTime.of(0, 15));
        fail("The test 'trainDepartureConstructorThrowsOnBlankTrainNumber' failed since "
                + "it did not throw 'Ill.Arg.Exc.' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Train number' was a blank string, "
                + "please retry registration.", e.getMessage());
      }
    }

    /**
     * Tests {@link TrainDeparture#setDelay(LocalTime)} for throwing 'Ill.Arg.Exc.' on blank delay.
     *
     * <p>Verifies that setDelay throws an Ill.Arg.Exc when attempting to set a blank delay.
     * It checks the expected exception message and ensures that the delay remains unchanged.
     *
     * @see TrainDeparture#setDelay(LocalTime)
     */
    @Test
    @DisplayName("TrainDeparture-setTrack throws 'Ill.Arg.Exc.' on negative track")
    public void trackSetterThrows() {
      // Arrange
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(12, 0), "Line2",
              "456", "Oslo", 2, LocalTime.of(1, 30));
      try {
        trainDeparture.setTrack(-5);
        fail("Expected IllegalArgumentException, but no exception was thrown");
      } catch (IllegalArgumentException e) {
        // Assert
        assertEquals("The parameter 'Track' must be a positive integer, from 0 - 4",
                e.getMessage());
        assertEquals(2, trainDeparture.getTrack());
      }
    }

    @Test
    @DisplayName("TrainDeparture-setDelay throws 'Ill.Arg.Exc.' on blank delay")
    public void delaySetterThrows() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(12, 0), "Line2",
              "456", "Oslo", 2, LocalTime.of(1, 30));
      try {
        trainDeparture.setDelay(null);
        fail("Expected IllegalArgumentException, but no exception was thrown");
      } catch (IllegalArgumentException e) {

        assertEquals("The LocalTime for the parameter 'delay' was null, "
                + "please provide a valid time.", e.getMessage());
        assertEquals(LocalTime.of(1, 30), trainDeparture.getDelay());
      }
    }

    /**
     * Tests {@link TrainDeparture#setDestination(String)} for throwing 'Ill.Arg.Exc.' with
     * excessive characters.
     *
     * <p>Verifies that setDestination throws an IllegalArgumentException when attempting to set a
     * destination with more than 24 characters. It checks the expected exception message and
     * ensures that the destination remains unchanged.
     *
     * @see TrainDeparture#setDestination(String)
     */
    @Test
    @DisplayName("TrainDeparture-setDestination throws 'Ill.Arg.Exc.' on to many characters")
    public void destinationSetterThrows() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(12, 0), "Line2",
              "456", "Oslo", 2, LocalTime.of(1, 30));
      try {
        trainDeparture.setDestination("qwerqwerqwerqwerqwerqwerr");
        fail("Expected IllegalArgumentException, but no exception was thrown");
      } catch (IllegalArgumentException e) {

        assertEquals("The string for the parameter 'Destination' must have less than 24"
                + " characters, please retry registration.", e.getMessage());
        assertEquals("Oslo", trainDeparture.getDestination());
      }
    }

  }

  /**
   * Nested class for positive tests, which check that methods do not throw exceptions
   * and return expected values.
   */
  @Nested
  @DisplayName("Positive tests for TrainDeparture methods")
  public class MethodsDoNotThrowsException {

    /**
     * Tests the TrainDeparture constructor for creating a train departure
     * without throwing 'Ill.Arg.Exc.'.
     *
     * <p>Verifies that the TrainDeparture constructor creates an instance without throwing
     * an IllegalArgumentException.
     * It checks the created instance for the expected values of departure time, line, train number,
     * destination, track, and delay.
     */
    @Test
    @DisplayName("TrainDeparture-constructor creates a train departure"
            + " without throwing 'Ill.Arg.Exc.'")
    public void trainDepartureConstructorDoesNotThrow() {
      try {
        LocalTime departureTime = LocalTime.of(12, 30);
        TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
                "123", "Destination1", 1, LocalTime.of(0, 15));
        assertEquals(departureTime, departure.getDepartureTime());
        assertEquals("Line1", departure.getLine());
        assertEquals("123", departure.getTrainNumber());
        assertEquals("Destination1", departure.getDestination());
        assertEquals(1, departure.getTrack());
        assertEquals(LocalTime.of(0, 15), departure.getDelay());
      } catch (IllegalArgumentException e) {
        fail("The test 'trainDepartureConstructorDoesNotThrow' failed"
                + " with the exception-message" + e.getMessage());
      }
    }

    /**
     * Test that checks if the TrainDeparture constructor
     * creates a train departure with different values.
     */
    @Test
    @DisplayName("TrainDeparture-constructor creates a train departure with different values")
    public void trainDepartureConstructorCreatesDifferentTrainDeparture() {
      try {
        LocalTime departureTime = LocalTime.of(14, 45);
        TrainDeparture departure = new TrainDeparture(departureTime, "Line2",
                "456", "Destination2", 2, LocalTime.of(1, 30));
        assertEquals(departureTime, departure.getDepartureTime());
        assertEquals("Line2", departure.getLine());
        assertEquals("456", departure.getTrainNumber());
        assertEquals("Destination2", departure.getDestination());
        assertEquals(2, departure.getTrack());
        assertEquals(LocalTime.of(1, 30), departure.getDelay());
      } catch (IllegalArgumentException e) {
        fail("The test 'trainDepartureConstructorCreatesDifferentTrainDeparture'"
                + " failed with the exception-message" + e.getMessage());
      }
    }

    /**
     * Tests {@link TrainDeparture#getDepartureTime()} for correct departure time retrieval.
     *
     * <p>Verifies that the getDepartureTime method in the TrainDeparture class returns the expected
     * departure time.
     */
    @Test
    @DisplayName("TrainDeparture-getDepartureTime returns the expected departure time")
    public void trainDepartureGetDepartureTimeReturnsCorrectValue() {
      LocalTime departureTime = LocalTime.of(12, 30);
      TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
              "123", "Destination1", 1, LocalTime.of(0, 15));
      assertEquals(departureTime, departure.getDepartureTime());
    }

    /**
     * Tests {@link TrainDeparture#getLine()} for correct train number retrieval.
     *
     * <p>Verifies that the getLine method in the TrainDeparture class returns the expected line.
     *
     * @see TrainDeparture#getLine()
     */
    @Test
    @DisplayName("TrainDeparture-getLine returns the expected line value")
    public void trainDepartureGetLineReturnsCorrectValue() {
      LocalTime departureTime = LocalTime.of(12, 30);
      TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
              "123", "Destination1", 1, LocalTime.of(0, 15));
      assertEquals("Line1", departure.getLine());
    }

    /**
     * Tests {@link TrainDeparture#getTrainNumber()} for correct train number retrieval.
     *
     * <p>Verifies that the getTrainNumber method in the TrainDeparture class returns the expected
     * train number value.
     *
     * @see TrainDeparture#getTrainNumber()
     * */
    @Test
    @DisplayName("TrainDeparture-getTrainNumber returns the expected train number value")
    public void trainDepartureGetTrainNumberReturnsCorrectValue() {
      LocalTime departureTime = LocalTime.of(12, 30);
      TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
              "123", "Destination1", 1, LocalTime.of(0, 15));
      assertEquals("123", departure.getTrainNumber());
    }

    /**
     * Tests {@link TrainDeparture#getDestination()} for correct destination retrieval.
     *
     * <p>Verifies that the getDestination method in the TrainDeparture class returns the expected
     * destination value.
     *
     * @see TrainDeparture#getDestination()
     */
    @Test
    @DisplayName("TrainDeparture-getDestination returns the expected destination value")
    public void trainDepartureGetDestinationReturnsCorrectValue() {
      LocalTime departureTime = LocalTime.of(12, 30);
      TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
              "123", "Destination1", 1, LocalTime.of(0, 15));
      assertEquals("Destination1", departure.getDestination());
    }

    /**
     * Tests {@link TrainDeparture#getDelay()} for correct delay retrieval.
     *
     * <p>Verifies that the getDelay method in the TrainDeparture class returns the expected delay.
     *
     * @see TrainDeparture#getDelay()
     */
    @Test
    @DisplayName("TrainDeparture-getDelay returns the expected delay value")
    public void trainDepartureGetDelayReturnsCorrectValue() {
      LocalTime departureTime = LocalTime.of(12, 30);
      TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
              "123", "Destination1", 1, LocalTime.of(0, 15));
      assertEquals(LocalTime.of(0, 15), departure.getDelay());
    }


    /**
     * Tests the {@link TrainDeparture#setTrack(int)} method for updating the 'track' value.
     *
     * <p>This test ensures that calling the setTrack method with a valid track value updates
     * the 'track' field without throwing an IllegalArgumentException. It covers the scenario of
     * creating a TrainDeparture instance, setting a new track, and verifying the expected outcome.
     *
     * @see TrainDeparture#setTrack(int)
     */
    @Test
    @DisplayName("TrainDeparture-setter for track updates track value without throwing "
            + "'Ill.Arg.Exc.'")
    public void trackSetterUpdatesTrackValueWithoutThrowing() {
      try {
        LocalTime departureTime = LocalTime.of(12, 30);
        TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
                "123", "Destination1", 1, LocalTime.of(0, 15));
        departure.setTrack(2);
        assertEquals(2, departure.getTrack());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception-message" + e.getMessage());
      }
    }

    /**
     * Tests the {@link TrainDeparture#setDelay(LocalTime)} method for updating the 'delay' value.
     *
     * <p>This test ensures that calling the setDelay method with a valid LocalTime value updates
     * the 'delay' field without throwing an IllegalArgumentException. It covers the scenario of
     * creating a TrainDeparture instance, setting a new delay, and verifying the expected outcome.
     *
     * @see TrainDeparture#setDelay(LocalTime) ()
     */
    @Test
    @DisplayName("TrainDeparture-setter for delay updates delay value without throwing "
            + "'Ill.Arg.Exc.'")
    public void delaySetterUpdatesDelayValueWithoutThrowing() {
      try {
        LocalTime departureTime = LocalTime.of(12, 30);
        TrainDeparture departure = new TrainDeparture(departureTime, "Line1",
                "123", "Destination1", 1, LocalTime.of(0, 15));
        departure.setDelay(LocalTime.of(1, 0));
        assertEquals(LocalTime.of(1, 0), departure.getDelay());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception-message" + e.getMessage());
      }
    }

  }
}
