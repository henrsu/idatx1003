package edu.ntnu.stud.view;


import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.stud.model.DepartureRegister;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalTime;
import java.util.Scanner;
import org.junit.jupiter.api.*;


/**
 * Test class for the UserInterface class.
 * Contains negative and positive tests for various methods from the UserInterface class.
 *
 * @author Henrik Sund
 *
 */
public class ScanTest  {
  private static DepartureRegister departureRegister;
  private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

  @BeforeEach
  void setUpStreams() {
    System.setOut(new PrintStream(outputStream));
  }

  @AfterEach
  void restoreStreams() {
    System.setOut(System.out);
  }

  @BeforeAll
  static void setUpAll() {
    Scan.setDepartureRegister(departureRegister);
  }

  @BeforeEach
  public void setUp() {
    departureRegister = new DepartureRegister();
    // Additional setup if needed
  }


  /**
   * Nested class for negative tests, which check if exceptions
   * are thrown on invalid input parameters.
   */
  @Nested
  @DisplayName("Negative tests for 'UserInterface', throws exceptions on wrong input parameters")
  public class NegativeTests {
    @Test
    void testScanLocalTimeNegative() {
      String inputValues = "invalid_time\n";
      InputStream inputStream = new ByteArrayInputStream(inputValues.getBytes());
      System.setIn(inputStream);


    }

    @Test
    void trainNumberNotPresentTest() {
      departureRegister.addDeparture(LocalTime.of(4, 0), "L2", "123",
              "Bergen", 2, LocalTime.of(0, 15));
      String inputValues = "1243\n";
      InputStream inputStream = new ByteArrayInputStream(inputValues.getBytes());
      System.setIn(inputStream);
      boolean result = departureRegister.isTrainNumberPresent((new Scanner(System.in)).next());
      assertFalse(result);
    }
  }

  /**
   * Nested class for positive tests, which check that methods do not throw exceptions
   * and return expected values.
   */
  @Nested
  @DisplayName("Positive tests for TrainDeparture methods")
  public class PositiveTests {

    @Test
    @DisplayName("Scan scanString() runs as expected with valid input\"")
    void scanStringNonBlankTest() {
      String inputValues = "Non-empty input\n";
      InputStream inputStream = new ByteArrayInputStream(inputValues.getBytes());
      System.setIn(inputStream);

      // Call the method that uses Scanner input
      String result = Scan.scanString();

      // Assert that the result is the non-empty input
      assertEquals("Non-empty input", result);
    }

    @Test
    @DisplayName("Scan scanString(int) runs as expected with valid input")
    void scanStringAllowedLengthTest() {
      String inputValues = "Oslo\n";
      InputStream inputStream = new ByteArrayInputStream(inputValues.getBytes());
      System.setIn(inputStream);

      // Call the method that uses Scanner input
      String result = Scan.scanString(7);

      // Assert that the result is the non-empty input
      assertEquals("Oslo", result);
    }

    @Test
    void trainNumberPresentTest() {
      departureRegister.addDeparture(LocalTime.of(4, 0), "L2", "123",
              "Bergen", 2, LocalTime.of(0, 15));
      String inputValues = "123\n";
      InputStream inputStream = new ByteArrayInputStream(inputValues.getBytes());
      System.setIn(inputStream);
      boolean result = departureRegister.isTrainNumberPresent((new Scanner(System.in)).next());
      assertTrue(result);
    }


    @Test
    @DisplayName("Scan scanLocalTime() returns the right input")
    void testScanLocalTimePositive() {
      // Simulate user input for a valid time (e.g., "12:30")
      String inputValues = "12:30\n";
      InputStream inputStream = new ByteArrayInputStream(inputValues.getBytes());
      System.setIn(inputStream);

      // Call the method that uses Scanner input
      LocalTime result = Scan.scanLocalTime();

      // Assertions for positive test
      assertNotNull(result);
      assertEquals(LocalTime.of(12, 30), result);
    }

    @Test
    @DisplayName("Scan scanInt() - Valid Integer")
    void testScanIntValid() {
      // Mock user input for a valid integer
      String inputValues = "3\n";
      InputStream inputStream = new ByteArrayInputStream(inputValues.getBytes());
      System.setIn(inputStream);

      // Call the method that uses Scanner input
      int result = Scan.scanInt();

      // Assert that the result is the valid integer
      assertEquals(3, result);
    }
  }
}

