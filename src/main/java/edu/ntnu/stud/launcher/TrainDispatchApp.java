package edu.ntnu.stud.launcher;

import edu.ntnu.stud.view.UserInterface;

/**
 * The main class for the train dispatch application.
 * This class contains the main method to start the application.
 *
 * @author Henrik Sund
 */
public class TrainDispatchApp {

  /**
   * Main-method that runs the application.
   *
   * @param args are the arguments for the main-method.
   */
  public static void main(String[] args) {
    UserInterface.init();
  }
}
