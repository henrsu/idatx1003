package edu.ntnu.stud.view;

import edu.ntnu.stud.model.DepartureRegister;
import edu.ntnu.stud.model.TrainDeparture;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/**
 * Utility class with static methods to receive user input to the run terminal
 * for a departure system. The class is designed to interact with a {@link DepartureRegister}
 * for validation and retrieval of information.
 *
 * <p>
 * The class is used to gather input from the user for actions such as registering departures,
 * editing delays, and managing the departure system. It ensures proper validation of user inputs
 * and provides guidance for retrying or returning to the main menu.
 * </p>
 *
 * @author Henrik Sund
 */

public class Scan {
  private static DepartureRegister departureRegister;
  static int trackUpperLimit = TrainDeparture.trackAmount;
  static int maxCharacters = TrainDeparture.maxCharacters;

  /**
   * Private constructor to ensure that no objects of the class Scan can be made by other classes.
   */
  private Scan() {}

  /**
   * Assigns a DepartureRegister object to the static variable 'departureRegister'.
   * This method allows external classes to update the departure register used by the system.
   *
   * @param register The DepartureRegister to be assigned.
   */
  public static void setDepartureRegister(DepartureRegister register) {
    departureRegister = register;
  }

  /**
   * Scans for user input, ensuring that the string is not empty and does not exceed the specified
   * maximum length.
   *
   * <p>
   * This method creates a new {@link Scanner} to gather the next line of input from the user.
   * If the input is empty, the method prints an error message and prompts the user to try again.
   * If the input is larger then the specified maximum length, the method prints an error message
   * with the character limit and prompts the user to try again.
   * </p>
   *
   * @param maxLength The maximum allowed length for the input string.
   * @return The user-inputted string.
   */
  protected static String scanString(int maxLength) {
    String out = "";
    while (out.isEmpty() || out.length() > maxLength) {
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        out = scan.nextLine();
      }
      if (out.isEmpty()) {
        System.out.println("The string was blank, please try again.");
      }
      if (out.length() > maxLength) {
        System.out.println("The departure of your inputted train number has to many characters,"
                + " the limit is " + maxLength + ".");
      }
    }
    return out;
  }

  /**
   * Scans for user input by checking that the string is not empty by creating a new
   * {@link Scanner} and fetching the next line inputted by the user.
   *
   * @return The string that the user has inputted.
   */
  protected static String scanString() {
    String out = "";
    while (out.isEmpty()) {
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        out = scan.nextLine();
      }
      if (out.isEmpty()) {
        System.out.println("The string was blank, please try again.");
      }

    }
    return out;
  }

  /**
   * Scans for a string related to a departure parameter, limiting the length to the specified
   * maximum character length.
   * Prompts the user to enter a string for the specified departure parameter and
   * validate that it does not exceed the given maximum length.
   *
   * @param parameterName The name of the parameter for which the string is being entered,
   *                     used for the print.
   * @param maxLength     The maximum length allowed for the entered string.
   * @return A string entered by the user for the specified departure parameter.
   */
  protected static String scanForDepartureString(String parameterName, int maxLength) {
    System.out.println("Please enter a string for the departure " + parameterName + ":");
    return scanString(maxLength);
  }

  /**
   * Scans for a valid and unique train number for departure registration.
   * Prompts the user to enter a train number, ensuring it is not empty, not already present
   * in the register, and does not exceed the specified maximum character limit.
   *
   * @return A valid and unique train number entered by the user for departure registration.
   */
  protected static String scanForDepartureTrainNumber() {
    String out = "";
    while (out.isEmpty() || departureRegister.isTrainNumberPresent(out)
            || (out.length() > maxCharacters)) {
      System.out.println("Please enter a valid and unique train number:");
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        out = scan.nextLine().trim();
        // You can add additional validation here if needed
        if (out.isEmpty()) {
          System.out.println("Train number cannot be empty. Please enter a valid train number.");
        }
        if (departureRegister.isTrainNumberPresent(out)) {
          System.out.println("Train number is already in the register. Enter a unique number");
        }
        if (out.length() > maxCharacters) {
          System.out.println("The departure of your inputted train number has to many characters,"
                  + " the limit is " + maxCharacters + ".");
        }
      }
    }
    return out;
  }

  /**
   * Scans for a valid train number from user input, ensuring that the input is not empty
   * and exists in the departure register. Continues prompting the user until a valid train number
   * is provided.
   *
   * @return A valid train number entered by the user.
   */
  protected static String scanForDepartureTrainNumberDelete() {
    String out = "";
    while (out.isEmpty() || !departureRegister.isTrainNumberPresent(out)) {
      System.out.println("Please enter a train number that is in the register:");
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        out = scan.nextLine().trim();
        if (out.isEmpty()) {
          System.out.println("Train number cannot be empty. Please enter a valid train number.");
        }
        if (departureRegister.isTrainNumberPresent(out)) {
          System.out.println("The departure of your inputted train number have been deleted");
        }
      }
    }
    return out;
  }

  /**
   * Scans for a valid train number from user input, not allowing that the input is not empty,
   * exists in the departure register, and does not exceed the maximum allowed characters.
   * Continues prompting the user until a valid train number is provided.
   *
   * @return A valid train number entered by the user.
   */
  protected static String scanForDepartureTrainNumberEdit() {
    String out = "";
    while (out.isEmpty() || !departureRegister.isTrainNumberPresent(out)
            || out.length() > maxCharacters) {
      System.out.println("Please enter a train number that is in the register:");
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        out = scan.nextLine().trim();
        // You can add additional validation here if needed
        if (out.isEmpty()) {
          System.out.println("Train number cannot be empty. Please enter a valid train number.");
        }
        if (departureRegister.isTrainNumberPresent(out)) {
          System.out.println("The departure of your inputted train number is in the register.");
        }
        if (out.length() > maxCharacters) {
          System.out.println("The departure of your inputted train number has to many characters,"
                  + " the limit is " + maxCharacters + ".");
        }
      }
    }
    return out;
  }

  /**
   * Scans for user input and includes user guidance that an integer is supposed to be entered.
   * Utilizes the util method {@link #scanInt()} to create the scanner.
   *
   * @return an integer which the scanInt method has returned.
   */
  protected static int scanForDepartureInt() {
    System.out.println("Please enter a integer value from 0-" + trackUpperLimit + " for the "
            + "departure track, 0 if track is unknown:");
    return scanInt();
  }

  /**
   * Reads a positive integer from the console input.
   * <p>
   * This method prompts the user to enter an integer. If the entered value is not a valid integer
   * or is less than or equal to zero, it displays appropriate error messages and prompts the user
   * again until a valid positive integer is provided.
   * </p>
   *
   * @return The positive integer that is eventually entered by the user.
   */
  protected static int scanInt() {
    int out = -1;
    String inStr = "";
    while (out < 0 || out > trackUpperLimit) {
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        inStr = scan.nextLine();
      }
      try {
        out = Integer.parseInt(inStr);
        if (out < 0) {
          System.out.println("The integer was less then zero, please enter a integer from 0-"
                  + trackUpperLimit + ".");
        } else if (out > trackUpperLimit) {
          System.out.println("The integer was more then " + trackUpperLimit + ", which is the "
                  + "last track, please enter a integer from 0-" + trackUpperLimit + ".");
        }
      } catch (NumberFormatException e) {
        System.out.println("The inputted string could not be parsed to an integer: "
                + e.getMessage());
      }
    }
    return out;
  }

  /**
   * Scans for user input and includes user guidance that a LocalTime is supposed to be entered.
   *
   * @param parameterName is a string describing the type of input expected,
   *                      e.g., 'departure time'.
   * @return a LocalTime which the scanLocalTime method has returned.
   */
  protected static LocalTime scanForDepartureLocalTime(String parameterName) {
    System.out.println("Please enter a valid time for the " + parameterName
            + ": (HH:mm)");
    return scanLocalTime();
  }

  /**
   * Reads a LocalTime from the console input.
   * <p>
   * This method prompts the user to enter a time in the format 'HH:mm'. If the entered value
   * is not a valid LocalTime, it displays appropriate error messages and prompts the user
   * again until a valid time is provided.
   * </p>
   *
   * @return The LocalTime that is eventually entered by the user.
   */
  protected static LocalTime scanLocalTime() {
    LocalTime out = null;
    String inStr = "";
    while (out == null) {
      Scanner scan = new Scanner(System.in);
      if (scan.hasNextLine()) {
        inStr = scan.nextLine();
      }
      try {
        out = LocalTime.parse(inStr);
      } catch (DateTimeParseException e) {
        System.out.println("The input is not to a valid time, use format (HH:mm): "
                + e.getMessage());
      }
    }
    return out;
  }


  /**
   * Prints user guidance that specifies that these actions are available for the user to input:
   * input string 'r' (or 'R') to retry the attempted registration, or
   * input any other string to return to main menu.
   *
   * @return true if the user inquires a retry, and false otherwise.
   */
  protected static boolean promptRetry() {
    System.out.println("Please input 'r' to retry, or anything else to return to main menu.");
    String choice = scanString();
    return choice.equalsIgnoreCase("r");
  }

}
