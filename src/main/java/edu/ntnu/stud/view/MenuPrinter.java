package edu.ntnu.stud.view;

/**
 * MenuPrinter class to print all menus displayed.
 * Goal: print information relevant to the user
 *
 * @author Henrik Sund
 */
public class MenuPrinter {
  //Static variables for frequently used strings
  private static final String OPTION_MESSAGE = """
        ________Please input one of the following options to continue:___
        """;
  private static final String ENTER = "\nPlease enter an option below:";

  /**
   * Private constructor to ensure that no other classes can create objects of the MenuPrinter
   * class.
   */
  private MenuPrinter() {}

  /**
   * Prints the user's options for input when in the main menu.
   */
  protected static void displayMainMenu() {
    System.out.println("""
        ________Welcome to Train Departure Menu__________
        """);
    System.out.println(OPTION_MESSAGE);
    System.out.println(" '1'  > View Train Departure Board");
    System.out.println(" '2'  > Search for a departure in the register");
    System.out.println(" '3'  > Add a departure to the register");
    System.out.println(" '4'  > Delete a departure from the register");
    System.out.println(" '5'  > Edit a departure in the register");
    System.out.println(" '6'  > Change time of day");
    System.out.println(" '7'  > Quit the Train Dispatch Application");
    System.out.println(ENTER);
  }

  /**
   * Prints the user's option for input when in the search menu.
   */
  protected static void displaySearchMenu() {
    System.out.println(OPTION_MESSAGE);
    System.out.println(" '1'  > Search for train number");
    System.out.println(" '2'  > Search for destination");
    System.out.println(" '3'  > Search for line");
    System.out.println(" '0'  > To return to the main menu");
    System.out.println(ENTER);
  }

  /**
   * Prints the user's options for input when editing the departure with the trainNumber.
   **/
  protected static void displayEditOptions() {
    System.out.println(OPTION_MESSAGE);
    System.out.println(" '1'  > Edit the track of a departure");
    System.out.println(" '2'  > Edit the delay of a departure");
    System.out.println(" '3'  > Edit the destination of a departure");
    System.out.println(" '0'  > To return to main menu");
    System.out.println(ENTER);
  }



}
