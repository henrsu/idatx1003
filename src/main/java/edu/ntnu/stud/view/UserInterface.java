package edu.ntnu.stud.view;

import edu.ntnu.stud.model.DepartureRegister;
import edu.ntnu.stud.model.TrainDeparture;
import java.time.LocalTime;
import java.util.Objects;

/**
 * UserInterface class to handle the interactions of the user.
 *
 * @author Henrik Sund
 */
public class UserInterface {
  private static DepartureRegister departureRegister;

  //Static variables for frequently used strings
  private static final String RETURN_MESSAGE =
          "Please type '0' to go back to the main menu:";

  //Static variables for the main menu
  private static final String PRINT_REGISTER = "1";
  private static final String SEARCH_FOR_DEPARTURE = "2";
  private static final String ADD_DEPARTURE = "3";
  private static final String DELETE_DEPARTURE = "4";
  private static final String EDIT_DEPARTURE = "5";
  private static final String CHANGE_TIME = "6";
  private static final String EXIT = "7";

  //Static variables for the search menu
  private static final String TRAIN_NUMBER_SEARCH = "1";
  private static final String DESTINATION_SEARCH = "2";
  private static final String LINE_SEARCH = "3";

  //Static variables for the edit menu
  private static final String EDIT_TRACK = "1";
  private static final String EDIT_DELAY = "2";
  private static final String EDIT_DESTINATION = "3";

  static int maxCharacters = TrainDeparture.maxCharacters;
  static int maxDestinationCharacters = TrainDeparture.maxDestinationCharacters;



  public UserInterface() {}


  private static void registerDefaultDepartures() {
    departureRegister.addDeparture(
              LocalTime.of(18, 0), // departureTime
              "L1",          // line
              "602",             // trainNumber
              "Oslo",            // destination
              1,                  // track
              LocalTime.of(0, 0) // delay
    );
    departureRegister.addDeparture(
              LocalTime.of(8, 55),  // departureTime
              "F4",               // line
              "45",                // trainNumber
              "Bergen",               // destination
              0,                     // track
              LocalTime.of(0, 15));      // delay
    departureRegister.addDeparture(
              LocalTime.of(12, 45),  // departureTime
              "F1",             // line
              "19",                // trainNumber
              "Trondheim",               // destination
              1,                     // track
              LocalTime.of(1, 0));
    departureRegister.addDeparture(
              LocalTime.of(12, 45),  // departureTime
              "L2",             // line
              "1951",                // trainNumber
              "Trondheim",               // destination
              2,                     // track
              LocalTime.of(1, 0));     // delay
    departureRegister.addDeparture(
              LocalTime.of(12, 45),  // departureTime
              "L2",             // line
              "195",                // trainNumber
              "Trondheim",               // destination
              1,                     // track
              LocalTime.of(0, 50));     // delay
    departureRegister.addDeparture(
              LocalTime.of(13, 45),  // departureTime
              "L2",             // line
              "1955",                // trainNumber
              "Trondheim",               // destination
              2,                     // track
              LocalTime.of(0, 15));     // delay
    departureRegister.addDeparture(
              LocalTime.of(13, 45),  // departureTime
              "1233332",             // line
              "1958232",                // trainNumber
              "Øvraørnefjeddstakkslåttå",               // destination
              1,                     // track
              LocalTime.of(0, 30));     // delay
  }

  /**
   * Runs the main menu by printing the main menu options. The method continuously prompts
   * the user for input and performs corresponding actions based on the selected menu option
   * until the user chooses to exit.
   *
   * <p>
   * The menu options include:
   * - Print all departures
   * - Search for a departure
   * - Add a new departure
   * - Delete a departure
   * - Edit a departure
   * - Change the time
   * - Exit the application
   * </p>
   * <p>
   * The method uses a loop to repeatedly display the main menu, read the user's choice,
   * and execute the corresponding functionality. It exits the loop and terminates the
   * application when the user chooses to exit.
   * </p>
   */
  static void start() {
    MenuPrinter.displayMainMenu();
    boolean exit = false;
    while (!exit) {
      String choice = Scan.scanString();
      switch (choice) {
        case PRINT_REGISTER -> displayDepartures();
        case SEARCH_FOR_DEPARTURE -> searchRegister();
        case ADD_DEPARTURE -> addDeparture();
        case DELETE_DEPARTURE -> deleteDeparture();
        case EDIT_DEPARTURE -> editDeparture();
        case CHANGE_TIME -> updateTime();
        case EXIT -> exit = true;
        default -> System.out.println("Invalid choice, chose from option 1 - 7");
      }
    }
    System.out.println("Exiting Train Dispatch Application...");
    System.exit(0);
  }

  /**
   * Launches the application by initializing the {@link #start()} method that runs the main.
   * .
   * <p>
   * When launching this methods ensure that the necessary
   * components are initialized before starting the main functionality. It performs the
   * following steps:
   * </p>
   * <ol>
   *   <li>Creates a new DepartureRegister and sets it as the global departure register.</li>
   *   <li>Sets the departure register for the scanner to allow input/output operations.</li>
   *   <li>Initializes the current time to (0, 0).</li>
   *   <li>Registers default departures </li>
   *   <li>Prints all departures currently in the register.</li>
   *   <li>Starts the main application loop, allowing users to interact with the system.</li>
   * </ol>
   */
  public static void init() {
    departureRegister = new DepartureRegister();
    Scan.setDepartureRegister(departureRegister);
    departureRegister.setUpdateCurrentTime(LocalTime.of(0, 0));
    registerDefaultDepartures();
    departureRegister.printAllDepartures();
    start();
  }

  /**
   * Prints a message to let the user know that the application is returning to the main menu.
   * Runs the {@link #start()} method to initiate the main menu.
   */
  private static void returnToMainMenu() {
    System.out.println("Returning to main menu...");
    start();
  }

  /**
   * Polls the user to input the string "0" to return to main menu. If the user inputs another
   * string, the instruction is re-printed.
   */
  private static void waitToReturn() {
    System.out.println(RETURN_MESSAGE);
    try {
      String input = Scan.scanString();
      assert input != null;
      if (input.equals("0")) {
        returnToMainMenu();
      } else {
        waitToReturn();
      }
    } catch (IllegalArgumentException e) {
      System.out.println("The scanner failed to get the input as a string.");
      waitToReturn();
    }
  }

  /**
   * Displays all departures in the register and waits for user input
   * to return to the main menu.
   <p>
   * Prints all departures currently registered in the departure register
   * using the {@link DepartureRegister#printAllDepartures()} method. After displaying
   * the departures, it calls {@link #waitToReturn()} method, allowing the user
   * to input '0' to return to the main menu.
   * </p>
   */
  private static void displayDepartures() {
    departureRegister.printAllDepartures();
    waitToReturn();
  }

  /**
   * Help method utilized in the {@link #searchRegister()} to guide the user on how to perform a
   * search for a departure in the register by the train number.
   *
   * @return false if the user wants to retry to search and true if the search was successful or if
   *         the user wishes to go the main menu.
   */
  public static boolean numberSearch() {
    System.out.println("Please enter a string to search for a Train number:");
    String searchNumber = Scan.scanString();
    if (departureRegister.isTrainNumberPresent(searchNumber)) {
      departureRegister.trainNumberSearch(searchNumber);
      waitToReturn();
      return true;
    } else {
      System.out.println("The register does not contain train number " + searchNumber);
      if (Scan.promptRetry()) {
        return false;
      } else {
        returnToMainMenu();
        return true;
      }
    }
  }

  /**
   * Help method utilized in the {@link #searchRegister()} to guide the user on how to perform a
   * search for a departure in the register by the destination.
   *
   * @return false if the user wants to retry to search and true if the search was successful or if
   *         the user wishes to go the main menu.
   */

  private static boolean destinationSearch() {
    System.out.println("Please enter a string to search for a destination:");
    String searchDescription = Scan.scanString();
    if (!departureRegister.isTrainDestinationPresent(searchDescription)) {
      System.out.println("The register did not contain any match for the search: \n"
              + searchDescription);
      if (Scan.promptRetry()) {
        return false;
      } else {
        returnToMainMenu();
        return true;
      }
    } else {
      System.out.println("The departures with destination " + searchDescription + "\n");
      departureRegister.trainDestinationSearch(searchDescription);
      waitToReturn();
      return true;
    }
  }


  /**
   * Prompts the user to enter a string for searching train departures by line.
   * If a match is found, displays the matching departures, otherwise, prompts
   * the user to retry or return to the main menu.
   *
   * @return True if the operation is completed, and false if the user chooses
   *          to return to the main menu or retries the search.
   */
  private static boolean lineSearch() {
    System.out.println("Please enter a string to search for a line:");
    String searchDescription = Scan.scanString();
    if (!departureRegister.isTrainLinePresent(searchDescription)) {
      System.out.println("The register did not contain any match for the search: \n"
              + searchDescription);
      if (Scan.promptRetry()) {
        return false;
      } else {
        returnToMainMenu();
        return true;
      }
    } else {
      System.out.println("The departures with line " + searchDescription + "\n");
      departureRegister.trainLineSearch(searchDescription);
      waitToReturn();
      return true;
    }
  }

  /**
   * Utilizes the Printer.displaySearchMenu() to print the search menu.
   * Thereafter, checks the user input according to the options of the search menu with a
   * switch-case. Each case prompts the user for additional input to satisfy the specified search.
   */
  private static void searchRegister() {
    boolean exit = false;
    boolean scanAgain = true;
    String searchType = "";
    while (!exit) {
      if (scanAgain) {
        MenuPrinter.displaySearchMenu();
        searchType = Scan.scanString();
      }
      switch (Objects.requireNonNull(searchType)) {

        case TRAIN_NUMBER_SEARCH -> scanAgain = numberSearch();

        case DESTINATION_SEARCH ->  scanAgain = destinationSearch();

        case LINE_SEARCH -> scanAgain = lineSearch();

        default -> exit = true;
      }
    }
    returnToMainMenu();
  }

  /**
   * Adds a new departure to the departure register based on user input. Prompts the user for the
   * different attributes needed when creating a departure.
   *
   * @throws IllegalArgumentException If the inputted data cannot be processed as a valid departure.
   * @throws NullPointerException     If the inputted data results in a null pointer when addition.
   */
  private static void addDepartureFromInput() throws IllegalArgumentException,
          NullPointerException {

    LocalTime departureTime = Scan.scanForDepartureLocalTime("departure time");
    String line = Scan.scanForDepartureString("line", maxCharacters);
    String trainNumber = Scan.scanForDepartureTrainNumber();
    String destination = Scan.scanForDepartureString("destination", maxDestinationCharacters);
    int track = Scan.scanForDepartureInt();
    LocalTime delay = Scan.scanForDepartureLocalTime("delay");
    departureRegister.addDeparture(departureTime, line, trainNumber, destination, track, delay);
  }

  /**
   * Utilizes the method {@link #addDepartureFromInput()} to take user input and add a new departure
   * to the register. This method is tried, and if the inputs fail, the message is printed and the
   * user can retry registration of a departure.
   */
  private static void addDeparture() {
    try {
      addDepartureFromInput();
      System.out.println("The following departure was registered: ");
      departureRegister.printDepartureBoard(departureRegister.getLastDeparture());
      waitToReturn();
    } catch (IllegalArgumentException | NullPointerException e) {
      System.out.println("The inputted data could not be entered as an"
              + " Departure in the register,\n"
              + "for the reason: " + e.getMessage());
      if (Scan.promptRetry()) {
        addDeparture();
      } else {
        returnToMainMenu();
      }
    }
  }

  /**
   * Deletes a departure from the departure register based on user input.
   *
   * @throws IllegalArgumentException If the inputted data cannot be processed as a valid departure.
   * @throws NullPointerException     If the inputted data results in a null pointer when deleting.
   */
  private static void deleteDepartureFromInput() throws IllegalArgumentException,
          NullPointerException {
    String trainNumber = Scan.scanForDepartureTrainNumberDelete();
    departureRegister.deleteDeparture(trainNumber);
  }

  /**
   * Deletes a departure, handling exceptions and providing user feedback.
   */
  private static void deleteDeparture() {
    try {
      deleteDepartureFromInput();
      System.out.println("The updated departure board: ");
      departureRegister.printAllDepartures();
      waitToReturn();
    } catch (IllegalArgumentException | NullPointerException e) {
      System.out.println("The inputted data could not be entered as an Departure in the register,\n"
              + "for the reason: " + e.getMessage());
      if (Scan.promptRetry()) {
        deleteDeparture();
      } else {
        returnToMainMenu();
      }
    }
  }

  /**
   * Edits the track of a departure based on user input for train number and new track.
   * Prints the updated departure board if the edit is successful.
   *
   * @return True if the edit is successful or if the user decides to return to the main menu,
   *         false if the user wants to retry the edit.
   */
  private static boolean editTrack() {
    String trainNumber = Scan.scanForDepartureTrainNumberEdit();
    int track = Scan.scanForDepartureInt();
    if (departureRegister.isTrainNumberPresent(trainNumber)) {
      try {
        departureRegister.editTrack(trainNumber, track);
        departureRegister.printAllDepartures();
        waitToReturn();
        return true;
      } catch (IllegalArgumentException e) {
        System.out.println("Error editing track: " + e.getMessage());
        if (Scan.promptRetry()) {
          return false;
        }
      }
    } else {
      returnToMainMenu();
    }
    return true;
  }

  /**
   * Edits the delay of a specific train departure provided train number is unique.
   * If successful, displays the updated departure board.
   *
   * @return True if the delay edit is successful or if the user opts to return to the main menu,
   *         false if the user chooses to retry the delay adjustment.
   */
  private static boolean editDelay() {
    String trainNumber = Scan.scanForDepartureTrainNumberEdit();
    LocalTime delay = Scan.scanForDepartureLocalTime("delay");

    try {
      if (departureRegister.isTrainNumberPresent(trainNumber)) {
        try {
          departureRegister.editDelay(trainNumber, delay);
          departureRegister.printAllDepartures();
          waitToReturn();
          return true;  // Return true if the operation was successful
        } catch (IllegalArgumentException e) {
          System.out.println("Error editing delay: " + e.getMessage());
          if (Scan.promptRetry()) {
            return false;
          } else {
            returnToMainMenu();
          }
        }
      } else {
        System.out.println("The register does not contain train number " + trainNumber);
        if (Scan.promptRetry()) {
          return false;
        } else {
          returnToMainMenu();
        }
      }
    } catch (IllegalArgumentException e) {
      System.out.println("Error: " + e.getMessage());
      if (Scan.promptRetry()) {
        return false;
      } else {
        returnToMainMenu();
      }
    }
    return true;
  }



  /**
   * Edits the destination of a train departure.
   *
   * @return {@code true} if the destination is successfully edited, {@code false} otherwise.
   */
  private static boolean editDestination() {
    String trainNumber = Scan.scanForDepartureTrainNumberEdit();
    String destination = Scan.scanForDepartureString("destination", maxDestinationCharacters);
    if (departureRegister.isTrainNumberPresent(trainNumber)) {
      departureRegister.editDestination(trainNumber, destination);
      departureRegister.printAllDepartures();
      waitToReturn();
      return true;
    } else {
      System.out.println("The register does not contain train number " + trainNumber);
      if (Scan.promptRetry()) {
        return false;
      } else {
        returnToMainMenu();
        return true;
      }
    }
  }


  /**
   * Edits various aspects of a train departure based on user choice.
   * Delegates what option is chosen and directs you to the method.
   */
  private static void editDeparture() {
    departureRegister.printAllDepartures();
    boolean exit = false;
    boolean scanAgain = true;
    String choice = "";
    while (!exit) {
      if (scanAgain) {
        MenuPrinter.displayEditOptions();
        choice = Scan.scanString();
      }
      switch (choice) {

        case EDIT_TRACK -> scanAgain = editTrack();

        case EDIT_DELAY ->  scanAgain = editDelay();

        case EDIT_DESTINATION -> scanAgain = editDestination();

        default -> exit = true;
      }
    }
    returnToMainMenu();
  }



  /**
   * Updates the current time of the departure register based on user input.
   * The method prompts the user to input a new current time, checks if it's later
   * than the existing current time, and updates the current time if valid. Then
   * updated time is displayed, and the user is prompted to return to the main menu.
   * If the input or update fails, appropriate error messages are displayed, and the
   * user is given the option to retry or return to the main menu.
   *
   * @throws IllegalArgumentException If the input time is invalid or if turning back
   *                                  time is attempted.
   */
  private static void updateTime() {
    LocalTime inputTime = Scan.scanForDepartureLocalTime("new current time");
    try {
      if (inputTime.isAfter(departureRegister.getUpdateCurrentTime())) {
        try {
          departureRegister.setUpdateCurrentTime(LocalTime.of(inputTime.getHour(),
                  inputTime.getMinute()));
          System.out.println("Time is updated to " + inputTime);
          waitToReturn();

        } catch (IllegalArgumentException e) {
          System.out.println("Error updating current time: " + e.getMessage());
          if (Scan.promptRetry()) {
            updateTime();
          } else {
            returnToMainMenu();
          }
        }
      } else {
        System.out.println("You can't turn back the time, current time: "
                + departureRegister.getUpdateCurrentTime());
        if (Scan.promptRetry()) {
          updateTime();
        } else {
          returnToMainMenu();
        }
      }
    } catch (IllegalArgumentException | NullPointerException e) {
      System.out.println("Invalid input: " + e.getMessage());
      if (Scan.promptRetry()) {
        updateTime();
      } else {
        returnToMainMenu();
      }
    }

  }

}
