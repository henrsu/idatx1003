package edu.ntnu.stud.model;

import java.time.LocalTime;

/**
 * Train Departure class represents a train departure with detailed information, including
 * departure time, line, train number, destination, track, and delay.
 *
 * <p>This class provides a structured way to store and manage information about train departures.
 * It includes methods for verifying parameters, setting and retrieving departure details, and
 * generating a string representation of the departure.
 *
 * <p>Usage:
 * <pre>
 * {@code
 * LocalTime departureTime = LocalTime.of(8, 0);
 * String line = "L1";
 * String trainNumber = "602";
 * String destination = "Oslo";
 * int track = 1;
 * LocalTime delay = LocalTime.of(0, 15);
 *
 * TrainDeparture departure = new TrainDeparture(departureTime, line,
 * trainNumber, destination, track, delay);
 * System.out.println(departure);
 * }
 * </pre>
 *
 * @author Henrik Sund
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final String trainNumber;
  private String destination;
  private int track;
  private LocalTime delay;
  private LocalTime adjustedDepartureTime;

  public static final int trackAmount = 4;
  public static final int maxCharacters  = 7;

  //The destination in norway most letters is "Øvraørnefjeddstakkslåttå" with 24 characters
  public static final int maxDestinationCharacters  = 24;

  /**
   * Verifies the length and content of a string parameter.
   * <p>
   * If the provided string parameter is blank or exceeds the specified maximum length.
   * If the string is blank, it throws an {@link IllegalArgumentException} with a message
   * indicating that the string should not be blank. If the string exceeds the specified maximum
   * length, it throws an {@link IllegalArgumentException} with a message indicating the maximum
   * allowed length.
   * </p>
   *
   * @param parameter      The string parameter to be verified.
   * @param parameterName  The name of the parameter for informative error messages.
   * @param maxLength      The maximum allowed length for the string parameter.
   * @throws IllegalArgumentException If the string is blank or exceeds the maximum length.
   */
  private void verifyStringParameterLength(String parameter, String parameterName, int maxLength)
          throws IllegalArgumentException {
    if (parameter.isBlank()) {
      throw new IllegalArgumentException("The string for the parameter '"
              + parameterName + "' was a blank string, please retry registration.");
    } else if (parameter.length() > maxLength) {
      throw new IllegalArgumentException("The string for the parameter '" + parameterName + "'"
              + " must have less than " + maxLength + " characters, please retry registration.");
    }
  }


  /**
   * Verifies the length and content of a general string parameter.
   * <p>
   * It delegates the verification to the {@link #verifyStringParameterLength(String, String, int)}
   * method with the specified maximum length for 'trainNumber' and 'line' string parameters.
   * </p>
   *
   * @param parameter      The string parameter to be verified.
   * @param parameterName  The name of the parameter for informative error messages.
   * @throws IllegalArgumentException If the string is blank or exceeds the maximum length.
   */
  private void verifyStringLength(String parameter, String parameterName)
          throws IllegalArgumentException {
    verifyStringParameterLength(parameter, parameterName, maxCharacters);
  }

  /**
   * Verifies the length and content of a destination string parameter.
   * <p>
   * It delegates the verification to the {@link #verifyStringParameterLength(String, String, int)}
   * method with the specified maximum length for destination string parameters.
   * </p>
   *
   * @param destination    The destination string parameter to be verified.
   * @throws IllegalArgumentException If the string is blank or exceeds the maximum length.
   */
  private void verifyStringDestinationLength(String destination) throws IllegalArgumentException {
    verifyStringParameterLength(destination, "Destination", maxDestinationCharacters);
  }



  /**
   * Verifies that a LocalTime parameter is not null.
   *
   * @param parameter      The LocalTime parameter to be verified.
   * @param parameterName  A String indicating the name or purpose of the parameter.
   * @throws IllegalArgumentException If the provided LocalTime parameter is null.
   */
  private void verifyLocalTimeParameter(LocalTime parameter, String parameterName)
          throws IllegalArgumentException {
    if (parameter == null) {
      throw new IllegalArgumentException("The LocalTime for the parameter '"
              + parameterName + "' was null, please provide a valid time.");
    }
  }



  /**
   * Verifies that an integer parameter is a positive integer.
   *
   * @param track     The integer track to be verified.
   * @throws IllegalArgumentException If the provided integer parameter is not a positive integer.
   */
  private void verifyPositiveTrack(int track)
          throws IllegalArgumentException {
    if (track < 0 || track > trackAmount) {
      throw new IllegalArgumentException("The parameter 'Track' must be a positive integer,"
              + " from 0 - " + trackAmount);
    }
  }


  /**
   * Constructor for train departures.
   *
   * @param departureTime Time of the departure
   * @param line Defines the distance the train travels
   * @param trainNumber Unique number for the train
   * @param destination Final destination for the train
   * @param track Track number
   * @param delay Delay for the train
   */
  public TrainDeparture(LocalTime departureTime, String line,
                        String trainNumber, String destination,
                        int track, LocalTime delay) throws IllegalArgumentException {
    verifyLocalTimeParameter(departureTime, "Departure Time");
    verifyStringLength(line, "Line");
    verifyStringLength(trainNumber, "Train number");
    verifyStringDestinationLength(destination);
    verifyPositiveTrack(track);
    verifyLocalTimeParameter(delay, "Delay");

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    // Adjust track to -1 if it's provided as 0
    this.track = (track == 0) ? -1 : track;
    this.delay = delay;
    this.adjustedDepartureTime = departureTime.plusHours(delay.getHour())
            .plusMinutes(delay.getMinute());
  }



  /**
   * Returns a LocalTime representing the departure time of this object of the TrainDeparture class.
   *
   * @return a LocalTime being the train departure time.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }


  /**
   * Returns a String with information on the line of this departure.
   *
   * @return a String with the line of this train departure.
   */
  public String getLine() {
    return line;
  }

  /**
   * Returns a String representing the train number of this train departure.
   *
   * @return a String with the train number of this train departure.
   */
  public String getTrainNumber() {
    return trainNumber;
  }

  /**
   * Returns a String with information on where the final destination of this departure is.
   *
   * @return a String with the destination of this train departure.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Returns an integer value of the track given to this TrainDeparture.
   *
   * @return an integer with the track number of the train departure.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Gets the track information for display. If the track is -1, returns an empty string;
   * otherwise, returns the track value as a string.
   *
   * @return The track information for display.
   */
  public String getTrackForDisplay() {
    if (getTrack() == -1) {
      return "";
    } else {
      return Integer.toString(getTrack());
    }
  }

  /**
   * Returns a LocalTime representing the departure delay of this TrainDeparture.
   *
   * @return a LocalTime being the train departure delay.
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Gets the delay information for display. If the delay is zero, returns an empty string;
   * otherwise, returns the delay value as a string in the "HH:mm" format.
   *
   * @return The delay information for display.
   */
  public String getDelayForDisplay() {
    if (getDelay().toString().equals("00:00")) {
      return "";
    } else {
      return getDelay().toString();
    }
  }

  /**
   * Returns a LocalTime representing the adjustedDepartureTime of this TrainDeparture,
   * adjustedDepartureTime is a combination of the previous departureTime addition with the delay.
   *
   * @return a LocalTime being the adjusted departure time for the train.
   */
  public LocalTime getAdjustedDepartureTime() {
    return adjustedDepartureTime;
  }

  /**
   * Sets the track number of the train.
   *
   * <p>This method updates the track number of the train departure.
   * If the provided track is 0, it is
   * adjusted to -1 to represent an undefined or non-existent track.
   *
   * @param track The new track number. Use 0 to represent an undefined or non-existent track.
   *
   * @throws IllegalArgumentException If the provided track is a negative integer.
   *
   * @see #getTrack()
   */
  public void setTrack(int track) {
    verifyPositiveTrack(track);
    this.track = (track == 0) ? -1 : track;
  }

  /**
   * Sets the delay for the train departure.
   *
   * <p>This method updates the delay for the train departure. The delay represents any additional
   * time beyond the scheduled departure time.
   *
   * @param delay The new delay to be set. Must be a non-null non-negative LocalTime value.
   *
   * @throws IllegalArgumentException If the provided delay is null or represents a negative time.
   *
   * @see #getDelay()
   */
  public void setDelay(LocalTime delay) {
    verifyLocalTimeParameter(delay, "delay");
    this.delay = delay;
    setAdjustedDepartureTime();
  }

  /**
   * Sets the destination for the train departure.
   *
   * <p>This method updates the destination for the train departure.
   *
   * @param destination The new destination to be set. Must be a non-null string.
   *
   * @throws IllegalArgumentException If the provided destination is null.
   *
   * @see #getDestination()
   */
  public void setDestination(String destination) {
    verifyStringDestinationLength(destination);
    this.destination = destination;

  }

  /**
   * Sets the adjusted departure time for the train departure.
   *
   * <p>This method calculates and updates the adjusted departure time based on the original
   * departure time and the delay.
   *
   * @throws NullPointerException If the original departure time or delay is null.
   *
   */
  public void setAdjustedDepartureTime() {
    this.adjustedDepartureTime = departureTime.plusHours(delay.getHour())
            .plusMinutes(delay.getMinute());
  }



  /**
   * Returns a string representation of the TrainDeparture object.
   *
   * @return A string containing information about the train departure.
   */
  @Override
  public String toString() {
    return String.format("Departure time=%s, line='%s', train number='%s', "
                    + "destination='%s', delay='%s', track=%d",
            departureTime, line, trainNumber, destination, delay, track);
  }

}
