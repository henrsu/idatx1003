package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The DepartureRegister class manages and provides operations for a collection of train departures.
 * It allows the addition of new departures, searching based on train number and destination,
 * and printing details of all departures.
 * <p>
 * Usage example:
 * <pre>
 * {@code
 * // Create a new DepartureRegister
 * DepartureRegister register = new DepartureRegister();
 *
 * // Add a new departure
 * register.addDeparture(LocalTime.of(8, 0), "L1", "602", "Oslo", 1, LocalTime.of(0, 15));
 *
 * // Print all departures
 * register.printAllDepartures();
 *
 * // Search for departures by train number
 * register.trainNumberSearch("602");
 *
 * // Search for departures by destination
 * register.trainDestinationSearch("Oslo");
 * }
 * </pre>
 * </p>
 *
 * @author Henrik Sund
 */
public class DepartureRegister {

  private  List<TrainDeparture> departureRegisterList;
  private static LocalTime currentTime = LocalTime.of(0, 0);

  public DepartureRegister() {
    this.departureRegisterList = new ArrayList<>();
  }

  public List<TrainDeparture> getDepartureRegisterList() {
    return departureRegisterList;
  }

  /**
   * Returns a boolean to represent if the train number is present in the current register.
   * Checks if there are any objects in the register with the same train number as the trainNumber.
   *
   * @param trainNumber The train number to check for in the register.
   * @return true if a departure with the same train number is present in the register,
   *         and false otherwise.
   */
  public boolean isTrainNumberPresent(String trainNumber) {
    return departureRegisterList.stream()
            .anyMatch(departure -> departure.getTrainNumber().equals(trainNumber));
  }

  /**
   * Help method that verifies that the trainNumber is in fact not in the current register.
   *
   * @param trainNumber The train number to be checked for its absence.
   * @throws IllegalArgumentException if a departure with the same train number
   *        is present in the register.
   */
  private void verifyTrainNumberAbsence(String trainNumber) throws IllegalArgumentException {
    if (isTrainNumberPresent(trainNumber)) {
      throw new IllegalArgumentException("A departure with the same train number is already in the "
              + "register Please try again with a different train number.");
    }
  }

  private boolean lineCollision(LocalTime departureTime, LocalTime delay, String line) {
    return departureRegisterList.stream()
            .anyMatch(departure -> departure.getAdjustedDepartureTime()
                    .equals(departureTime.plusHours(delay.getHour())
                    .plusMinutes(delay.getMinute())) && departure.getLine().equals(line));
  }

  private void verifyLine(LocalTime departureTime, LocalTime delay, String line)
          throws IllegalArgumentException {
    if (lineCollision(departureTime, delay, line)) {
      throw new IllegalArgumentException("A departure with the same line and adjusted departureTime"
              + "is already in the register Please try again with a different "
              + "departure time, line or delay.");
    }
  }


  private boolean trackCollision(LocalTime departureTime, LocalTime delay, int track) {
    return departureRegisterList.stream()
            .anyMatch(departure -> departure.getAdjustedDepartureTime()
                    .equals(departureTime.plusHours(delay.getHour())
                            .plusMinutes(delay.getMinute())) && departure.getTrack() == track);
  }

  private void verifyTrack(LocalTime departureTime, LocalTime delay, int track)
          throws IllegalArgumentException {
    if (trackCollision(departureTime, delay, track)) {
      throw new IllegalArgumentException("A departure with the same track and adjusted"
              + " departureTime is already in the register Please try again with a different "
              + "departure time, line or delay.");
    }
  }

  /**
   * Adds a new train departure to the register.
   *
   * <p>This method creates a new {@link TrainDeparture} object with the provided details and
   * adds it to the departure register list. Before adding, it verifies that no departure with
   * the same train number already exists in the register.
   *
   * @param departureTime The departure time of the train.
   * @param line The line representing the distance the train travels.
   * @param trainNumber The unique number of the train.
   * @param destination The final destination of the train.
   * @param track The track number of the train.
   * @param delay The delay of the train.
   * @throws IllegalArgumentException If a train departure with the same train number already exists
   *                                  in the register.
   */
  public void addDeparture(LocalTime departureTime, String line,
                           String trainNumber, String destination,
                           int track, LocalTime delay) throws IllegalArgumentException {
    verifyTrainNumberAbsence(trainNumber);
    verifyLine(departureTime, delay, line);
    verifyTrack(departureTime, delay, track);
    TrainDeparture newDeparture = new TrainDeparture(departureTime, line, trainNumber,
            destination, track, delay);
    departureRegisterList.add(newDeparture);
  }

  /**
   * Sorts the list of train departures in the departure register based on various criteria.
   * <p>
   * Create a new sorted list of train departures from the existing departure register list.
   * The sorting is performed in the following  order:
   * </p>
   * <ol>
   *   <li>Departure Time: Departures sorted by departure times.</li>
   *   <li>Delay: If departure times are equal, departures are then sorted by delay.</li>
   *   <li>Track: If departure times and delays are equal, sorted by track number.</li>
   *   <li>Destination: If all previous criteria are equal,sorted by destination.</li>
   * </ol>
   * <p>
   * The sorted list is then assigned back to the departure register list, updating
   * the order of train departures within the register.
   * </p>
   */
  public void sortDepartureRegisterList() {
    List<TrainDeparture> sortedDepartures = departureRegisterList.stream()
            .sorted(Comparator.comparing(TrainDeparture::getDepartureTime)
                    .thenComparing(TrainDeparture::getDelay)
                    .thenComparing(TrainDeparture::getTrack)
                    .thenComparing(TrainDeparture::getDestination))
            .toList();
    departureRegisterList = new ArrayList<>(sortedDepartures);
  }

  /**
   * Removes train departures that have already departed, then sort the register.
   * Prints the Train Departure Board, including current departures, formatted in a table.
   */
  public void printAllDepartures() {
    removePastDepartures(); // removes train departures that have already departed
    sortDepartureRegisterList(); // Sort the register of departures
    printDepartureBoard(departureRegisterList); // Displays the register in the terminal
  }

  /**
   * Prints a formatted departure board based on the provided list of train departures.
   * <p>
   * Takes a list of {@link TrainDeparture} objects and prints a formatted train
   * departure board in the terminal. The departure board includes headers for departure time, line,
   * train number, destination, delay, and track. Each train departure's details are printed in a
   * tabular format.
   * </p>
   *
   * @param departures The list of train departures to be displayed on the departure board.
   */
  public void printDepartureBoard(List<TrainDeparture> departures) {

    String boldText = "\u001B[1m";
    String resetFormatting = "\u001B[0m";

    String horizontalLine = "+------------------------------------------------"
            + "------------------------+--------------------+";
    String outerFormat = "| %-74s | %-22s |";
    String innerFormat = "| %-15s | %-8s | %-13s | %-25s | %-10s | %-5s |";

    // Print header line
    System.out.println(horizontalLine);
    System.out.printf(outerFormat + "%n", boldText + "Train Departure Board",
             "Time: " + currentTime + resetFormatting);
    System.out.println(horizontalLine);
    System.out.printf(innerFormat + "%n", "Departure time", "Line",
             "Train number", "Destination", "Delay", "Track", "Departure Time With Delay");
    System.out.println(horizontalLine);

    // Print departures
    departures
            .forEach(departure -> System.out.printf(innerFormat + "%n",
                    departure.getDepartureTime(),
                    departure.getLine(),
                    departure.getTrainNumber(),
                    departure.getDestination(),
                    departure.getDelayForDisplay(),
                    departure.getTrackForDisplay()));

    // Print footer line
    System.out.println(horizontalLine);
  }



  /**
   * Prints details of train departures with a specific train number.
   *
   * @param trainNumber The train number to search for.
   * @throws IllegalArgumentException If no train with the specified train number is found.
   *                                   This exception is thrown when the provided trainNumber
   *                                   does not match any existing train in the register.
   */
  public void trainNumberSearch(String trainNumber) throws IllegalArgumentException {
    if (!isTrainNumberPresent(trainNumber)) {
      throw new IllegalArgumentException("No train with the specified train number exists in the "
              + "register. Please try again with a different train number.");
    } else {
      List<TrainDeparture> searchedDepartures = departureRegisterList.stream()
              .filter(departure -> departure.getTrainNumber().equals(trainNumber))
              .collect(Collectors.toList());

      printDepartureBoard(searchedDepartures);
    }
  }


  /**
   * Returns a boolean to represent if the destination is present in the current register.
   * Checks if there are any departures in the register with the same destination.
   *
   * @param destination The destination to check for in the register.
   * @return true if a departure with the same destination is present in the register,
   *         otherwise it is false.
   */
  public boolean isTrainDestinationPresent(String destination) {
    return departureRegisterList.stream()
            .anyMatch(departure -> departure.getDestination().equalsIgnoreCase(destination));
  }


  /**
   * Searches and prints details of train departures with a specific destination.
   *
   * <p>
   * This method uses streams to filter and then print train departures that have the specified
   * destination. If no matching departure is found, it throws an {@link IllegalArgumentException}.
   * </p>
   *
   * @param destination The destination to search for in the register.
   * @throws IllegalArgumentException If no train with the specified destination is found in the
   *                                  register. This exception is thrown when the provided
   *                                  destination does not match any existing destination
   *                                  in the register.
   */
  public void trainDestinationSearch(String destination) throws IllegalArgumentException {
    if (isTrainDestinationPresent(destination)) {
      List<TrainDeparture> searchedDepartures = departureRegisterList.stream()
              .filter(departure -> departure.getDestination().equalsIgnoreCase(destination))
              .collect(Collectors.toList());

      printDepartureBoard(searchedDepartures);
    } else {
      throw new IllegalArgumentException("No train with the specified destination exists in the "
              + "register. Please try again with a different destination.");
    }
  }

  /**
   * Checks if a train with the specified line is present in the departure register.
   *
   * @param line The line of the train to be checked.0
   * @return {@code true} if a train with the specified line is found, {@code false} otherwise.
   */
  public boolean isTrainLinePresent(String line) {
    return departureRegisterList.stream()
            .anyMatch(departure -> departure.getLine().equals(line));
  }

  /**
   * Searches for trains with the specified line in the departure register and prints the details
   * of matching train departures.
   *
   * @param line The line to search for in the departure register.
   * @throws IllegalArgumentException If no train with the specified line is found in the register.
   */
  public void trainLineSearch(String line) throws IllegalArgumentException {
    if (isTrainLinePresent(line)) {
      List<TrainDeparture> searchedDepartures = departureRegisterList.stream()
              .filter(departure -> departure.getLine().equals(line))
              .collect(Collectors.toList());

      printDepartureBoard(searchedDepartures);
    } else {
      throw new IllegalArgumentException("No train with the specified line exists in the "
              + "register. Please try again with a different line.");
    }
  }

  /**
   * Gets the time.
   *
   * <p>This method returns the current time as stored in the DepartureRegister.
   *
   * @return The current time.
   */

  public LocalTime getUpdateCurrentTime() {
    return DepartureRegister.currentTime;
  }

  /**
   * Updates the current time to a new specified time.
   * <p>
   * Allows for changing the time to a later time, and throws an exception if an
   * attempt is made to set the time to an earlier value, turning back the time.
   * </p>
   *
   * @param newTime The new time to set as the current time.
   * @throws IllegalArgumentException If the new time is earlier than the current time.
   *
   */
  public void setUpdateCurrentTime(LocalTime newTime) throws IllegalArgumentException {
    if (currentTime.isAfter(newTime)) {
      throw new IllegalArgumentException("You can't turn back the time, current time is: "
              + currentTime);
    } else {
      DepartureRegister.currentTime = newTime;
    }
  }




  /**
   * Removes train departures that have already departed.
   *
   * <p>This method filters the list of train departures to retain only those with adjusted
   * departure times after the current time or equal to the current time. The updated list
   *  then replace the previous departure register list.
   */
  public void removePastDepartures() {
    /* Filter departures to retain only those with adjusted departure times after
     or equal to the current time */
    List<TrainDeparture> filteredDepartures = departureRegisterList.stream()
            .filter(departure -> departure.getAdjustedDepartureTime().isAfter(currentTime)
                    || departure.getAdjustedDepartureTime().equals(currentTime))
            .toList();
    // Update the departure register list with the filtered departures
    departureRegisterList = new ArrayList<>(filteredDepartures);

  }


  /**
   * Retrieves the last registered TrainDeparture in the departure register.
   *
   * @return A List containing the last registered TrainDeparture.
   * @throws IllegalArgumentException if the departure register is empty.
   */
  public List<TrainDeparture> getLastDeparture() {
    List<TrainDeparture> lastDepartureList = new ArrayList<>();

    if (departureRegisterList.isEmpty()) {
      throw new IllegalArgumentException("The departure register is empty");
    } else {
      lastDepartureList.add(departureRegisterList.get(departureRegisterList.size() - 1));
    }
    return lastDepartureList;
  }

  /**
   * Deletes a train departure from the register based on the given train number.
   * The method filters out the train departure with the specified train number
   * from the departure register list and updates the departure register accordingly.
   *
   * @param trainNumber The train number of the departure to be deleted.
   * @throws IllegalArgumentException if the provided train number is not found
   *                                  in the departure register.
   */
  public void deleteDeparture(String trainNumber) {
    List<TrainDeparture> filteredDepartures = departureRegisterList.stream()
            .filter(departure -> !departure.getTrainNumber().equals(trainNumber))
            .toList();
    departureRegisterList = new ArrayList<>(filteredDepartures);
  }



  /**
   * Edits the track of a departure by the given train number.
   *
   * @param trainNumber The train number of the departure to be edited.
   * @param track       The new track value to be set.
   * @throws IllegalArgumentException if a departure with the same track and adjusted
   *                                  departureTime is already in the register.
   */
  public void editTrack(String trainNumber, int track) throws IllegalArgumentException {
    departureRegisterList.stream()
            .filter(departure -> departure.getTrainNumber().equals(trainNumber))
            .forEach(departure -> {
              LocalTime departureTime = departure.getDepartureTime();
              LocalTime delay = departure.getDelay();
              if (trackCollision(departureTime, delay, track)) {
                throw new IllegalArgumentException("A departure with the same track and"
                        + " adjusted departureTime is already in the register. \n"
                        + "Try again with a different departure time, track, or delay.");
              } else {
                departure.setTrack(track);
              }
            });
  }


  /**
   * Edits the destination of a train departure based on the provided train number and
   * new destination.
   *
   * @param trainNumber  The train number of the departure to be edited.
   * @param destination  The new destination to be set.
   */
  public void editDestination(String trainNumber, String destination) {
    departureRegisterList.stream()
            .filter(departure -> departure.getTrainNumber().equals(trainNumber))
            .findFirst()
            .ifPresent(departure -> departure.setDestination(destination));
  }


  /**
   * Edits the delay of a train departure based on the provided train number and new delay.
   *
   * @param trainNumber The train number of the departure to be edited.
   * @param delay       The new delay to be set.
   */
  public void editDelay(String trainNumber, LocalTime delay) throws IllegalArgumentException {
    departureRegisterList.stream()
            .filter(departure -> departure.getTrainNumber().equals(trainNumber))
            .forEach(departure -> {
              LocalTime departureTime = departure.getDepartureTime();
              String line = departure.getLine();
              int track = departure.getTrack();

              if (lineCollision(departureTime, delay, line)) {
                throw new IllegalArgumentException("A departure with the same line and"
                        + " adjusted departureTime is already in the register. \n"
                        + "Try again with a different departure time, line, or delay.");
              } else if (trackCollision(departureTime, delay, track)) {
                throw new IllegalArgumentException("A departure with the same track and"
                        + " adjusted departureTime is already in the register. \n"
                        + "Try again with a different departure time, track, or delay.");
              } else {
                departure.setDelay(delay);
              }
            });
  }


}
